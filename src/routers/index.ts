import { Express } from "express";
import { loginRouter } from "./login.router";
import { userRouter } from "./user.router";

export const initializerRouter = (app: Express) => {
  app.use("/api/user", userRouter());
  app.use("/api/login", loginRouter());
};
