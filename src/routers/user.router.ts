import { Router } from "express";
import {
  registerUser,
  listUsers,
  listUserById,
  alterUser,
} from "../controllers/user.controller";
import { validate } from "../middlewares/validation.middleware";
import { CreateUserSchema, UpdateUserSchema } from "../schemas/user.schema";
import { isAuthenticated } from "../middlewares/authentication.middleware";
import {
  isAdmCheck,
  isUserClientCheck,
  isOwnerCheck,
} from "../middlewares/authorization.middleware";

const router = Router();

export const userRouter = () => {
  router.post("", validate(CreateUserSchema), registerUser);
  router.get("", isAuthenticated, isAdmCheck, listUsers);
  router.get("/:id", isAuthenticated, isAdmCheck, listUserById);
  router.patch("/:id", isAuthenticated, isAdmCheck, alterUser);

  return router;
};
