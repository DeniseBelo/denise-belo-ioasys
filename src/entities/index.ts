import { User } from "./User";
import { Movie } from "./Movie";
import { UserMovies } from "./UserMovies";

export { User, Movie, UserMovies };
