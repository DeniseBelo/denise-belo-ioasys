import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { User } from "./User";

@Entity()
export class Movie {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  name!: string;

  @Column()
  genre!: string;

  @Column()
  director!: string;

  @Column()
  actors!: string;

  @Column()
  rate!: number;

  @ManyToOne((type) => User)
  @JoinColumn()
  user!: User;
}
