import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from "typeorm";
import { User } from "./User";
import { Movie } from "./Movie";

@Entity()
export class UserMovies {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @ManyToOne((type) => User)
  @JoinColumn()
  user!: User;

  @ManyToOne((type) => Movie)
  @JoinColumn()
  movie!: Movie;
}
