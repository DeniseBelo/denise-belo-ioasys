import { Request, Response, NextFunction } from "express";
import {
  createUser,
  getAllUsers,
  getUserById,
  updateUser,
} from "../services/user.service";

export const registerUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await createUser(req.validatedFields);
    res.status(201).json(user);
  } catch (error) {
    next(error);
  }
};

export const listUsers = async (req: Request, res: Response) => {
  const users = await getAllUsers();

  res.status(200).json(users);
};

export const listUserById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { id } = req.params;

  try {
    const user = await getUserById(id);
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};

export const alterUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { id } = req.params;
  const data = req.body;
  try {
    const updatedUser = await updateUser(id, data);
    res.status(200).json(updatedUser);
  } catch (error) {
    next(error);
  }
};
