import { Request, Response, NextFunction } from "express";
import { userLogin } from "../services/login.service";

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = await userLogin(req.body);
    res.status(200).json({ token });
  } catch (error) {
    next(error);
  }
};
