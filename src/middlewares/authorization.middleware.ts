import { Request, Response, NextFunction } from "express";
import AppError from "../errors/AppError";
import { getRepository } from "typeorm";
import { Movie, User } from "../entities";

export const isAdmCheck = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.isAuthorized) {
    return next();
  }

  const userRepository = getRepository(User);

  try {
    const user = await userRepository.findOne(req.currentUser.id);

    if (user?.is_admin === true) {
      next();
    } else {
      next(new AppError("No authorization", 401));
    }
  } catch (error) {
    next(new AppError("No authorization", 401));
  }
};

export const isUserClientCheck = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.isAuthorized) {
    return next();
  }

  const userRepository = getRepository(User);

  try {
    const user = await userRepository.findOne(req.currentUser.id);

    if (user?.is_admin === false) {
      next();
    } else {
      next(new AppError("No authorization", 401));
    }
  } catch (error) {
    next(new AppError("No authorization", 401));
  }
};

export const isOwnerCheck = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const owner = req.currentUser;
  const { id } = req.params;
  if (req.currentUser?.id === id) {
    req.isOwner = true;
  }
  next();
};
