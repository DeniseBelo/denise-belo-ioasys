import * as yup from "yup";

export const CreateUserSchema = yup.object().shape({
  first_name: yup.string().required(),
  last_name: yup.string().required(),
  is_admin: yup.bool().required(),
  email: yup.string().email().required(),
  password: yup.string().min(8).required(),
});

export const UpdateUserSchema = yup.object().shape({
  first_name: yup.string().required(),
  last_name: yup.string().required(),
  email: yup.string().email(),
  password: yup.string().min(8),
});
