import * as yup from "yup";

export const CreateMovieSchema = yup.object().shape({
  name: yup.string().required(),
  genre: yup.string().required(),
  director: yup.string().required(),
  actors: yup.string().required(),
  rate: yup.number().required(),
  description: yup.string().required(),
  image: yup.string().required(),
});
