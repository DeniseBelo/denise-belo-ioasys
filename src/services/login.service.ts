import { getRepository } from "typeorm";
import { User } from "../entities";
import AppError from "../errors/AppError";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

interface ILoginData {
  email: string;
  password: string;
}

export const userLogin = async (data: ILoginData) => {
  const { email, password } = data;
  const userRepository = getRepository(User);
  const user = await userRepository.findOne({ email });

  if (!user) {
    throw new AppError("User has not been registered yet.", 401);
  }

  const userPassword = await userRepository.findOne({
    select: ["password"],
    where: { email },
  });

  const match = bcrypt.compareSync(password, userPassword!.password);

  if (!match) {
    throw new AppError("User email and password missmatch", 401);
  }

  let token = jwt.sign({ user: user }, process.env.JWT_SECRET as string, {
    expiresIn: process.env.JWT_EXPIRESIN,
  });

  return token;
};
