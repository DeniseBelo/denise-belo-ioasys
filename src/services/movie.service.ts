import { getRepository } from "typeorm";
import { User } from "../entities";
import { Movie } from "../entities";
import AppError from "../errors/AppError";

interface IRegisterMovieBody {
  name: string;
  genre: string;
  director: boolean;
  actors: string;
  rate: number;
  user: User;
}

interface IUpdateMovieBody {
  name?: string;
  genre?: string;
  director?: boolean;
  actors?: string;
  rate?: number;
  user?: User;
}

const removeUser = (movie: Movie) => {
  const { id, name, genre, director, actors, rate } = movie;

  const newMovie = {
    id,
    name,
    genre,
    director,
    actors,
    rate,
    userId: movie.user.id,
  };

  return newMovie;
};

export const createMovie = async (data: IRegisterMovieBody, userID: string) => {
  try {
    const movieRepository = getRepository(Movie);
    const userRepository = getRepository(User);

    const user = await userRepository.findOne(userID);

    if (user === undefined) {
      throw new AppError("You don't have user permissions.", 400);
    }

    const movieExists = await movieRepository.findOne({
      where: { user: user, name: data.name },
    });

    if (movieExists) {
      throw new AppError("This movie is already registered.", 400);
    }

    const movie = movieRepository.create({ ...data, user: user });

    await movieRepository.save(movie);

    return removeUser(movie);
  } catch (error) {
    throw new AppError((error as any).message, 400);
  }
};
