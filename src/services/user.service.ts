import { getRepository } from "typeorm";
import { User } from "../entities";
import AppError from "../errors/AppError";
import bcrypt from "bcrypt";

interface IRegisterBody {
  first_name: string;
  last_name: string;
  is_admin: boolean;
  email: string;
  password: string;
}

interface IUpdateBody {
  first_name?: string;
  last_name?: string;
  is_active?: boolean;
  email?: string;
  password?: string;
}

const removePassword = (userInfos: User) => {
  const { id, first_name, last_name, is_admin, email } = userInfos;

  const userNoPassword = { id, first_name, last_name, is_admin, email };

  return userNoPassword;
};

export const createUser = async (body: IRegisterBody) => {
  try {
    const userRepository = getRepository(User);

    const user = userRepository.create(body);

    await userRepository.save(user);

    const userNoPassword = removePassword(user);

    return userNoPassword;
  } catch (error) {
    throw new AppError((error as any).message, 400);
  }
};

export const getAllUsers = async () => {
  const userRepository = getRepository(User);

  const users = await userRepository.find();

  const usersNoPassword = users.map((user) => removePassword(user));

  return usersNoPassword;
};

export const getUserById = async (userId: string) => {
  const userRepository = getRepository(User);

  try {
    const user = await userRepository.findOne(userId);

    if (user !== undefined) {
      const userNoPassword = removePassword(user);

      return userNoPassword;
    } else {
      throw new AppError("User not found", 404);
    }
  } catch (error) {
    throw new AppError("User not found.", 404);
  }
};

export const updateUser = async (userId: string, data: IUpdateBody) => {
  const userRepository = getRepository(User);
  const user = await userRepository.findOne({ id: userId });
  if (!user) {
    throw new AppError("User not found!", 404);
  }

  if (data.first_name) {
    const userData = { first_name: data.first_name };
    await userRepository.update(userId, userData);
  }

  if (data.last_name) {
    const userData = { last_name: data.last_name };
    await userRepository.update(userId, userData);
  }

  if (data.is_active) {
    const userData = { is_active: data.is_active };
    await userRepository.update(userId, userData);
  }

  if (data.email) {
    const userData = { email: data.email };
    await userRepository.update(userId, userData);
  }

  if (data.password) {
    const userData = { password: data.password };
    await userRepository.update(user.id, {
      password: bcrypt.hashSync(data.password, 8),
    });
  }

  return await userRepository.findOne({ id: userId });
};
