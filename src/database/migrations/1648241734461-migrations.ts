import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1648241734461 implements MigrationInterface {
    name = 'migrations1648241734461'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" DROP CONSTRAINT "FK_96b520bb829e52afb206a0644e4"`);
        await queryRunner.query(`ALTER TABLE "movie" RENAME COLUMN "userAdminId" TO "userId"`);
        await queryRunner.query(`CREATE TABLE "user_movies" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "userId" uuid, "moviesId" uuid, CONSTRAINT "PK_907a29c02ccac473d188dad7fb7" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "movie" ADD CONSTRAINT "FK_ec7ed42b2e89092919129bdf990" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_movies" ADD CONSTRAINT "FK_149d8bac146ea70af063a84e5dd" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_movies" ADD CONSTRAINT "FK_4a21a0b593454cceb637806c062" FOREIGN KEY ("moviesId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_movies" DROP CONSTRAINT "FK_4a21a0b593454cceb637806c062"`);
        await queryRunner.query(`ALTER TABLE "user_movies" DROP CONSTRAINT "FK_149d8bac146ea70af063a84e5dd"`);
        await queryRunner.query(`ALTER TABLE "movie" DROP CONSTRAINT "FK_ec7ed42b2e89092919129bdf990"`);
        await queryRunner.query(`DROP TABLE "user_movies"`);
        await queryRunner.query(`ALTER TABLE "movie" RENAME COLUMN "userId" TO "userAdminId"`);
        await queryRunner.query(`ALTER TABLE "movie" ADD CONSTRAINT "FK_96b520bb829e52afb206a0644e4" FOREIGN KEY ("userAdminId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
