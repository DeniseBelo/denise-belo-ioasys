import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1648242872171 implements MigrationInterface {
    name = 'migrations1648242872171'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "is_active" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "is_active"`);
    }

}
