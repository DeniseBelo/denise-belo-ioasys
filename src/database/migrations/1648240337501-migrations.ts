import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1648240337501 implements MigrationInterface {
    name = 'migrations1648240337501'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "is_admin" boolean NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "movie" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "genre" character varying NOT NULL, "director" character varying NOT NULL, "actors" character varying NOT NULL, "rate" integer NOT NULL, "userAdminId" uuid, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "movie_users_user" ("movieId" uuid NOT NULL, "userId" uuid NOT NULL, CONSTRAINT "PK_ac4b57222178f167371b8dd85d6" PRIMARY KEY ("movieId", "userId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_bba8437b4847401b4dfbe50eaf" ON "movie_users_user" ("movieId") `);
        await queryRunner.query(`CREATE INDEX "IDX_535c3f5e0daed572029af7cf66" ON "movie_users_user" ("userId") `);
        await queryRunner.query(`ALTER TABLE "movie" ADD CONSTRAINT "FK_96b520bb829e52afb206a0644e4" FOREIGN KEY ("userAdminId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "movie_users_user" ADD CONSTRAINT "FK_bba8437b4847401b4dfbe50eaf4" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "movie_users_user" ADD CONSTRAINT "FK_535c3f5e0daed572029af7cf668" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie_users_user" DROP CONSTRAINT "FK_535c3f5e0daed572029af7cf668"`);
        await queryRunner.query(`ALTER TABLE "movie_users_user" DROP CONSTRAINT "FK_bba8437b4847401b4dfbe50eaf4"`);
        await queryRunner.query(`ALTER TABLE "movie" DROP CONSTRAINT "FK_96b520bb829e52afb206a0644e4"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_535c3f5e0daed572029af7cf66"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_bba8437b4847401b4dfbe50eaf"`);
        await queryRunner.query(`DROP TABLE "movie_users_user"`);
        await queryRunner.query(`DROP TABLE "movie"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }

}
