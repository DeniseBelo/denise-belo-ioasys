import {MigrationInterface, QueryRunner} from "typeorm";

export class migrations1648243013526 implements MigrationInterface {
    name = 'migrations1648243013526'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_movies" DROP CONSTRAINT "FK_4a21a0b593454cceb637806c062"`);
        await queryRunner.query(`ALTER TABLE "user_movies" RENAME COLUMN "moviesId" TO "movieId"`);
        await queryRunner.query(`ALTER TABLE "user_movies" ADD CONSTRAINT "FK_4da20f03f7173c061971a1d96c0" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_movies" DROP CONSTRAINT "FK_4da20f03f7173c061971a1d96c0"`);
        await queryRunner.query(`ALTER TABLE "user_movies" RENAME COLUMN "movieId" TO "moviesId"`);
        await queryRunner.query(`ALTER TABLE "user_movies" ADD CONSTRAINT "FK_4a21a0b593454cceb637806c062" FOREIGN KEY ("moviesId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
