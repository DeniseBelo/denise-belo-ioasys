# Ioasys API

This API is an application for a movie platform. It allows the client to register, list, list by id, update and soft delete itself, and should also allows to register, update, filter and delete movies.

## How to get started (locally):

To run this service in your local you need to do some steps, starting with cloning this project from bitcucket. Click on this link:

```
https://bitbucket.org/denisebelo/teste-backend-denise/src/master/
```

Clone with the ssh option. After that, create a new directory in your local, inside this directory use:

```
git clone <paste the url copied>
```

Move in the directory created and run you code editor (you can use visual studio code, for instance).
Now open you terminal inside de project and if you have yarn previously installed in you machine, run:

```
yarn install
```

You will install all the dependencies you need this way. And now you can see the code.

In case you don't have yarn installed, follow these steps:

Yarn installation. In your terminal type the following code:

```
npm install --global yarn
```

With yarn installed go to the directory of the project:

```
cd <your_project>
```

Run the code on your local. Open a terminal inside your project directory, and install the project dependencies:

```
yarn install
```

Run the service locally:

```
yarn dev
```

Now you have to use insomnia or other platform to access the API. Configure this platform with you localhost address in the port 3000.

- the service will be running at http://127.0.0.1:3000/

## Routes:

These are the endpoints you will need to access the API:

# User:

The following endpoints will manage the clients features:

## POST /api/user

Create a new user.

- Does not need authentication.

### Request example:

```
{
	"first_name": "Joao",
	"last_name": "Carlos",
	"is_admin": true,
	"email": "joao@mail.com",
	"password": "12345678"
}
```

### Response example:

- STATUS: 201 CREATED

```
{
  "id": "31722930-6c76-4c38-951e-8f3b600aee1d",
  "first_name": "Joao",
  "last_name": "Carlos",
  "is_admin": true,
  "email": "joao1@mail.com"
}
```

## POST /api/login

Generate login token that will be necessary to others endpoints.

- Does not need authentication.

### Request example:

```
{
	"email": "denise@mail.com",
	"password": "12345678"
}
```

### Response example:

- STATUS: 200 OK

```
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoiZmFhYzBjNjgtZGViNy00MGEzLThlYmItOWIwY2ZkNjFhOGJjIiwiZmlyc3RfbmFtZSI6IkRlbmlzZSIsImxhc3RfbmFtZSI6IkJlbG8iLCJpc19hZG1pbiI6dHJ1ZSwiaXNfYWN0aXZlIjp0cnVlLCJlbWFpbCI6ImRlbmlzZUBtYWlsLmNvbSIsInBhc3N3b3JkIjoiJDJiJDA4JGFxZVl0OGllZElxVFBlRUt5ZkJ5RHVQenREUmpjQlBncjdvcVFnQ2FQendXV2lzWHMubGhTIn0sImlhdCI6MTY0ODI2MjM5NywiZXhwIjoxNjQ4MjY1OTk3fQ._v87r-Bur79_Erm4fB1XW0O_uhIlYCwBKG1zyrXfnDM"
}
```

## Get /api/user

List all the users registered.

- Need admin authentication.
- Bearer token: admin login token.

### Request example:

```
No body
```

### Response example:

- STATUS: 200 OK

```
[
  {
    "id": "6863dd56-be23-46ea-8090-95efab12d0a3",
    "first_name": "Bruno",
    "last_name": "Rocha",
    "is_admin": false,
    "email": "bruno@mail.com"
  },
  {
    "id": "1d02eaba-bf04-4fdb-8308-95ed597b1de0",
    "first_name": "Joao",
    "last_name": "Carlos",
    "is_admin": false,
    "email": "joao@mail.com"
  },
]
```

## GET /api/user/<user_id>

List one user filtered by the id.

- Need admin authentication.
- Bearer token: admin login token.
- Use the user id in the route's parameter url.

### Request example:

```
no body
```

### Response example:

- STATUS: 200 OK

```
{
    "id": "406d6712-dac9-469b-b9d1-a84499c5f533",
    "first_name": "Denise",
    "last_name": "Belo",
    "is_admin": true,
    "email": "denise3@mail.com"
  },
```

## Patch /api/user/<user_id>

This route update one user information and also allows the soft delete by turning attribute is_active to false.

- Need admin authentication.
- Bearer token: admin login token.
- Use the user id in the route's parameter url.

### Request example:

```
{
	"first_name": "nome atualizado",
	"last_name": "atualizado",
	"is_active": true,
	"password": "12345678"
}
```

### Response example:

- STATUS: 200 OK

```
{
  "id": "95752e8f-ed61-4e19-9f44-6b2f130edabe",
  "first_name": "nome atualizado",
  "last_name": "atualizado",
  "is_admin": true,
  "is_active": true,
  "email": "denise2@mail.com",
  "password": "$2b$08$RV5z4NFvCLMkiNt7kMoBM.lKjEojNr7C7KFhf.N9JlADVDMSFWUjm"
}
```

## Technologies

- Node.js;
- Typescript;
- Express;
- Yup;
- Postgres;

## Licenses

MIT
